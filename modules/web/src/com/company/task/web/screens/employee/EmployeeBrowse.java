package com.company.task.web.screens.employee;

import com.haulmont.cuba.gui.screen.*;
import com.company.task.entity.Employee;

@UiController("task_Employee.browse")
@UiDescriptor("employee-browse.xml")
@LookupComponent("employeesTable")
@LoadDataBeforeShow
public class EmployeeBrowse extends StandardLookup<Employee> {
    public void someMethod() {
        //some actions
    }
}
