package com.company.task.web.screens.employee;

import com.haulmont.cuba.gui.screen.*;
import com.company.task.entity.Employee;

@UiController("task_Employee.edit")
@UiDescriptor("employee-edit.xml")
@EditedEntityContainer("employeeDc")
@LoadDataBeforeShow
public class EmployeeEdit extends StandardEditor<Employee> {
}