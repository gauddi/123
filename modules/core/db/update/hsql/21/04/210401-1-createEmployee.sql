create table TASK_EMPLOYEE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(255) not null,
    MIDDLE_NAME varchar(255) not null,
    LAST_NAME varchar(255) not null,
    EMAIL_ADRESS varchar(255) not null,
    PHONE_NUMBER varchar(255) not null,
    POST varchar(255) not null,
    COMPANY varchar(255),
    PASSWORD varchar(255),
    --
    primary key (ID)
);